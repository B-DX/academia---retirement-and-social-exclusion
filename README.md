To obtain results from regression analyses shown in article:

Wetzel, Martin & Mahne, Katharina (2016) "Out of society? Retirement affects perceived social exclusion in Germany" published in [Zeitschrift für Gerontologie und Geriatrie - Volume 4](https://www.springerpflege.de/out-of-society-retirement-affects-perceived-social-exclusion-in-/10669350).

1. Request Panel Study Labour Market and Social Security (PASS) data from here: https://fdz.iab.de/de/FDZ_Individual_Data/PASS.aspx

2. Use the "Wetzel-Mahne_ZFGG.do" dofile written in Stata 10 for data preparation as well descriptive (Table 1) and inference analyses (Table).





<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

You are free to:

  * Share — copy and redistribute the material in any medium or format
  * Adapt — remix, transform, and build upon the material
  * for any purpose, even commercially.

This license is acceptable for Free Cultural Works.

  * The licensor cannot revoke these freedoms as long as you follow the license terms.

Under the following terms:

  * Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
  * ShareAlike — If you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original.
  * No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.
